class EventHub {}

const listeners = {};

EventHub.$on = function(event, callback) {
    if (!listeners[event]) listeners[event] = [];
    listeners[event].push(callback);
};

EventHub.$off = function(event, callback) {
    if (!listeners[event] || !Array.isArray(listeners[event])) return;
    if (!callback) {
        delete listeners[event];
    } else {
        for (let i = listeners[event].length - 1; i >= 0; --i) {
            if (listeners[event][i] === callback) listeners[event].splice(i, 1);
        }
    }
};

EventHub.$emit = function() {
    const args = [...arguments];
    const event = args.shift();

    if (!listeners[event] || !Array.isArray(listeners[event])) return;
    for (const listener of listeners[event]) {
        (args.length > 0) ? listener(...args) : listener();
    }
};

export default {
    install: (app, options) => {
        app.config.globalProperties.$eventHub = EventHub;
    }
};
