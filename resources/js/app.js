import { createApp } from 'vue';

const app = createApp({});

app.use(require('./plugins/EventHub').default);

app.component('checkbox', require('./components/Checkbox.vue').default);
app.component('color-picker', require('./components/ColorPicker.vue').default);
app.component('log', require('./components/Log.vue').default);
app.component('snack-tray', require('./components/SnackTray.vue').default);
app.component('theme-picker', require('./components/ThemePicker.vue').default);
app.component('tracker', require('./components/Tracker.vue').default);

app.mount('#app');
