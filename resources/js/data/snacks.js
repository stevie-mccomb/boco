export default [
    {
        name: 'Xelphatol Apple',
        icon: '/img/xelphatol-apple.png',
        value: { red: 5, green: -5, blue: -5 }
    },

    {
        name: 'Mamook Pear',
        icon: '/img/mamook-pear.png',
        value: { red: -5, green: 5, blue: -5 }
    },

    {
        name: 'O\'Ghomoro Berries',
        icon: '/img/oghomoro-berries.png',
        value: { red: -5, green: -5, blue: 5 }
    },

    {
        name: 'Doman Plum',
        icon: '/img/doman-plum.png',
        value: { red: -5, green: 5, blue: 5 }
    },

    {
        name: 'Valfruit',
        icon: '/img/valfruit.png',
        value: { red: 5, green: -5, blue: 5 }
    },

    {
        name: 'Cieldalaes Pineapple',
        icon: '/img/cieldalaes-pineapple.png',
        value: { red: 5, green: 5, blue: -5 }
    },

    {
        name: 'Han Lemon',
        icon: '/img/han-lemon.png',
        value: { red: 0, green: 0, blue: 0 }
    }
];
