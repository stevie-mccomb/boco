export const colors = [
    {
        name: 'Snow White',
        value: { red: 228, green: 223, blue: 208 }
    },

    {
        name: 'Ash Grey',
        value: { red: 172, green: 168, blue: 162 }
    },

    {
        name: 'Goobbue Grey',
        value: { red: 137, green: 135, blue: 132 }
    },

    {
        name: 'Slate Grey',
        value: { red: 101, green: 101, blue: 101 }
    },

    {
        name: 'Charcoal Grey',
        value: { red: 72, green: 71, blue: 66 }
    },

    {
        name: 'Soot Black',
        value: { red: 43, green: 41, blue: 35 }
    },

    {
        name: 'Rose Pink',
        value: { red: 230, green: 159, blue: 150 }
    },

    {
        name: 'Lilac Purple',
        value: { red: 131, green: 105, blue: 105 }
    },

    {
        name: 'Rolanberry Red',
        value: { red: 91, green: 23, blue: 41 }
    },

    {
        name: 'Dalamud Red',
        value: { red: 120, green: 26, blue: 26 }
    },

    {
        name: 'Rust Red',
        value: { red: 98, green: 34, blue: 7 }
    },

    {
        name: 'Wine Red',
        value: { red: 69, green: 21, blue: 17 }
    },

    {
        name: 'Coral Pink',
        value: { red: 204, green: 108, blue: 94 }
    },

    {
        name: 'Blood Red',
        value: { red: 145, green: 59, blue: 48 }
    },

    {
        name: 'Salmon Pink',
        value: { red: 228, green: 170, blue: 138 }
    },

    {
        name: 'Sunset Orange',
        value: { red: 183, green: 92, blue: 45 }
    },

    {
        name: 'Mesa Red',
        value: { red: 125, green: 57, blue: 6 }
    },

    {
        name: 'Bark Brown',
        value: { red: 106, green: 75, blue: 55 }
    },

    {
        name: 'Chocolate Brown',
        value: { red: 110, green: 61, blue: 36 }
    },

    {
        name: 'Russet Brown',
        value: { red: 79, green: 45, blue: 31 }
    },

    {
        name: 'Kobold Brown',
        value: { red: 48, green: 33, blue: 27 }
    },

    {
        name: 'Cork Brown',
        value: { red: 201, green: 145, blue: 86 }
    },

    {
        name: 'Qiqirn Brown',
        value: { red: 153, green: 110, blue: 63 }
    },

    {
        color: 'orange',
        name: 'Opo-Opo Brown',
        value: { red: 123, green: 92, blue: 45 }
    },

    {
        name: 'Aldgoat Brown',
        value: { red: 162, green: 135, blue: 92 }
    },

    {
        name: 'Pumpkin Orange',
        value: { red: 197, green: 116, blue: 36 }
    },

    {
        name: 'Acorn Brown',
        value: { red: 142, green: 88, blue: 27 }
    },

    {
        name: 'Orchard Brown',
        value: { red: 100, green: 66, blue: 22 }
    },

    {
        name: 'Chestnut Brown',
        value: { red: 61, green: 41, blue: 13 }
    },

    {
        name: 'Gobbiebag Brown',
        value: { red: 185, green: 164, blue: 137 }
    },

    {
        name: 'Shale Brown',
        value: { red: 146, green: 129, blue: 108 }
    },

    {
        name: 'Mole Brown',
        value: { red: 97, green: 82, blue: 69 }
    },

    {
        name: 'Loam Brown',
        value: { red: 63, green: 51, blue: 41 }
    },

    {
        name: 'Bone White',
        value: { red: 235, green: 211, blue: 160 }
    },

    {
        name: 'Ul Brown',
        value: { red: 183, green: 163, blue: 112 }
    },

    {
        name: 'Desert Yellow (Default)',
        value: { red: 219, green: 180, blue: 87 }
    },

    {
        name: 'Honey Yellow',
        value: { red: 250, green: 198, blue: 43 }
    },

    {
        name: 'Millioncorn Yellow',
        value: { red: 228, green: 158, blue: 52 }
    },

    {
        name: 'Coeurl Yellow',
        value: { red: 188, green: 136, blue: 4 }
    },

    {
        name: 'Cream Yellow',
        value: { red: 242, green: 215, blue: 112 }
    },

    {
        name: 'Halatali Yellow',
        value: { red: 165, green: 132, blue: 48 }
    },

    {
        name: 'Raisin Brown',
        value: { red: 64, green: 51, blue: 17 }
    },

    {
        name: 'Mud Green',
        value: { red: 88, green: 82, blue: 48 }
    },

    {
        name: 'Sylph Green',
        value: { red: 187, green: 187, blue: 138 }
    },

    {
        name: 'Lime Green',
        value: { red: 171, green: 176, blue: 84 }
    },

    {
        name: 'Moss Green',
        value: { red: 112, green: 115, blue: 38 }
    },

    {
        name: 'Meadow Green',
        value: { red: 139, green: 156, blue: 99 }
    },

    {
        name: 'Olive Green',
        value: { red: 75, green: 82, blue: 50 }
    },

    {
        name: 'Marsh Green',
        value: { red: 50, green: 54, blue: 33 }
    },

    {
        name: 'Apple Green',
        value: { red: 149, green: 174, blue: 92 }
    },

    {
        name: 'Cactuar Green',
        value: { red: 101, green: 130, blue: 65 }
    },

    {
        name: 'Hunter Green',
        value: { red: 40, green: 75, blue: 38 }
    },

    {
        name: 'Ochu Green',
        value: { red: 64, green: 99, blue: 57 }
    },

    {
        name: 'Adamantoise Green',
        value: { red: 95, green: 117, blue: 88 }
    },

    {
        name: 'Nophica Green',
        value: { red: 59, green: 77, blue: 60 }
    },

    {
        name: 'Deepwood Green',
        value: { red: 30, green: 42, blue: 33 }
    },

    {
        name: 'Celeste Green',
        value: { red: 150, green: 189, blue: 185 }
    },

    {
        name: 'Turquoise Green',
        value: { red: 67, green: 114, blue: 144 }
    },

    {
        name: 'Morbol Green',
        value: { red: 31, green: 70, blue: 70 }
    },

    {
        name: 'Ice Blue',
        value: { red: 178, green: 196, blue: 206 }
    },

    {
        name: 'Sky Blue',
        value: { red: 131, green: 176, blue: 210 }
    },

    {
        name: 'Seafog Blue',
        value: { red: 100, green: 129, blue: 160 }
    },

    {
        name: 'Peacock Blue',
        value: { red: 59, green: 104, blue: 134 }
    },

    {
        name: 'Rhotano Blue',
        value: { red: 28, green: 61, blue: 84 }
    },

    {
        name: 'Corpse Blue',
        value: { red: 142, green: 155, blue: 172 }
    },

    {
        name: 'Ceruleam Blue',
        value: { red: 79, green: 87, blue: 102 }
    },

    {
        name: 'Woad Blue',
        value: { red: 47, green: 56, blue: 81 }
    },

    {
        name: 'Ink Blue',
        value: { red: 26, green: 31, blue: 39 }
    },

    {
        name: 'Raptor Blue',
        value: { red: 91, green: 127, blue: 192 }
    },

    {
        name: 'Othard Blue',
        value: { red: 47, green: 88, blue: 137 }
    },

    {
        name: 'Storm Blue',
        value: { red: 35, green: 65, blue: 114 }
    },

    {
        name: 'Void Blue',
        value: { red: 17, green: 41, blue: 68 }
    },

    {
        name: 'Royal Blue',
        value: { red: 39, green: 48, blue: 103 }
    },

    {
        name: 'Midnight Blue',
        value: { red: 24, green: 25, blue: 55 }
    },

    {
        name: 'Shadow Blue',
        value: { red: 55, green: 55, blue: 71 }
    },

    {
        name: 'Abyssal Blue',
        value: { red: 49, green: 45, blue: 87 }
    },

    {
        name: 'Lavender Purple',
        value: { red: 135, green: 127, blue: 174 }
    },

    {
        name: 'Gloom Purple',
        value: { red: 81, green: 69, blue: 96 }
    },

    {
        name: 'Currant Purple',
        value: { red: 50, green: 44, blue: 59 }
    },

    {
        name: 'Iris Purple',
        value: { red: 183, green: 158, blue: 188 }
    },

    {
        name: 'Grape Purple',
        value: { red: 59, green: 42, blue: 61 }
    },

    {
        name: 'Lotus Pink',
        value: { red: 254, green: 206, blue: 245 }
    },

    {
        name: 'Colibri Pink',
        value: { red: 220, green: 155, blue: 202 }
    },

    {
        name: 'Plum Purple',
        value: { red: 121, green: 82, blue: 108 }
    },

    {
        name: 'Regal Purple',
        value: { red: 102, green: 48, blue: 78 }
    }
];

export const closest = function(given) {
    let closestDistance = null;
    let closestColor = null;

    for (const color of colors) {
        const redDistance = (given.value.red - color.value.red) * (given.value.red - color.value.red);
        const greenDistance = (given.value.green - color.value.green) * (given.value.green - color.value.green);
        const blueDistance = (given.value.blue - color.value.blue) * (given.value.blue - color.value.blue);
        const distance = Math.sqrt(redDistance + greenDistance + blueDistance);
        if (closestDistance === null || distance < closestDistance) {
            closestDistance = distance;
            closestColor = color;
        }
    }

    return closestColor;
};

export default { colors, closest };
